resource "aws_ecs_service" "devops_exercise_service_db" {
  name            = "devops-exercise-service-db"
  cluster         = aws_ecs_cluster.devops_exercise_cluster_db.id
  task_definition = aws_ecs_task_definition.devops_exercise_db_taskdef.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.task_db_sg.id]
    subnets         = aws_subnet.private.*.id
  }
}

resource "aws_security_group" "task_db_sg" {
  name   = "devops-exercise-task-db-sg"
  vpc_id = aws_vpc.devops_exercise_vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = 5432
    to_port         = 5432
    security_groups = [aws_security_group.task_sg.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
