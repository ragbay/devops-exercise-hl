data "aws_iam_role" "ecs_task_execution_role" {
  name = aws_iam_role.ecs_task_exec_role.name
}

resource "aws_ecs_task_definition" "devops_exercise_taskdef" {
  family                   = "devops-exercise-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn

  container_definitions = <<DEFINITION
[
  {
    "image": "077288845992.dkr.ecr.us-west-2.amazonaws.com/devops-exercise-app:latest",
    "cpu": 1024,
    "memory": 2048,
    "name": "devops-exercise-app",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80
      }
    ]
  }
]
DEFINITION
}
