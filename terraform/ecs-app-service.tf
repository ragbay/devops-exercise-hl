resource "aws_ecs_service" "devops_exercise_service" {
  name            = "devops-exercise-service"
  cluster         = aws_ecs_cluster.devops_exercise_cluster_app.id
  task_definition = aws_ecs_task_definition.devops_exercise_taskdef.arn
  desired_count   = 3
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.task_sg.id]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.devops_exercise_tg.id
    container_name   = "devops-exercise-app"
    container_port   = 80
  }

  depends_on = [aws_lb_listener.devops_exercise_listener]
}

resource "aws_security_group" "task_sg" {
  name   = "devops-exercise-task-sg"
  vpc_id = aws_vpc.devops_exercise_vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
