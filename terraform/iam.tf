
resource "aws_iam_role" "ecs_task_exec_role" {
  name               = "devops-exercise-ecsTaskExecutionRole"
  description        = "Managed by Terraform"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_exec_role_policy_document.json
}

data "aws_iam_policy_document" "ecs_task_exec_role_policy_document" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "AmazonECSTaskExecutionRolePolicy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy" "AmazonSSMReadOnlyAccess" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "ecs-task-exec-role-policy-attach" {
  role       = aws_iam_role.ecs_task_exec_role.name
  policy_arn = data.aws_iam_policy.AmazonECSTaskExecutionRolePolicy.arn
}

resource "aws_iam_role_policy_attachment" "ssm-readonly-role-policy-attach" {
  role       = aws_iam_role.ecs_task_exec_role.name
  policy_arn = data.aws_iam_policy.AmazonSSMReadOnlyAccess.arn
}