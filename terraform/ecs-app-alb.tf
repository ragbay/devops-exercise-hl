resource "aws_lb" "devops_exercise_alb" {
  name            = "devops-exercise-alb"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.lb.id]
}

resource "aws_lb_target_group" "devops_exercise_tg" {
  name        = "devops-exercise-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.devops_exercise_vpc.id
  target_type = "ip"
}

resource "aws_lb_listener" "devops_exercise_listener" {
  load_balancer_arn = aws_lb.devops_exercise_alb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.devops_exercise_tg.id
    type             = "forward"
  }
}

resource "aws_security_group" "lb" {
  name   = "devops-exercise-alb-security-group"
  vpc_id = aws_vpc.devops_exercise_vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Add this for https support
  #   ingress {
  #     protocol    = "tcp"
  #     from_port   = 443
  #     to_port     = 443
  #     cidr_blocks = ["0.0.0.0/0"]
  #   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
