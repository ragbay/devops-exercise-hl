# DevOps-Exercise-HL

Terraform ECS Deployment for Nginx, PHP and PostgreSQL

## terraform/provider.tf
- terraform aws provider for AWS credentials and region

## terraform/vpc.tf
 - For VPC Resources:
 - 3 Availability Zones; 6 subnets; 3 public and 3 private subnets; One private and public subnets for each AZ
 - data block to get available AZ in the AWS account/region
 - NAT Gateway resource will allow resources to communicate within VPC
 - Internet Gateway resource to allow resources to communicate between vpc and internet
 - CIDR Block: 10.1.0.0/16

## terraform/iam.tf
 - IAM Role needed to execute ECS Tasks and Read from SSM Parameter Store

## terraform/ecs-app-alb.tf
 - Internet facing Application Load Balancer to distribute traffic across the web application tasks
 - ALB has a listener/target group to forward port 80 traffic to ECS
 - ALB security group will only allow port 80 traffic from sources anywhere. 

## terraform/ecs-app-task-definition.tf
 - Defined to use FARGATE(AWS managed ECS host) with specified cpu/memory allocation.  
 - Container mage pulled from Amazon ECR based on the Dockerfile information below. it was built to use a custom static page that queries from PostgreSQL database
 - the Docker container exposes the PHP app on port 80
 - network mode is set to "awsvpc", which will have an elastic network interface and a private IP address  assigned to the task when it runs

## terraform/ecs-app-cluster.tf
 - ECS cluster resource definition for the app

## terraform/ecs-app-service.tf
 - ECS service specifies the number of tasks the app should be run with the task_definition and desired_count properties within the cluster
 - ECS service dependency on the ALB has been configured to make sure the ALB exists first (depends_on)
 - Security group created for ECS service, to allow only traffic from load balancer over tcp port 80 

## terraform/output.tf
 - added output to show the Load Balancer URL at the end of terraform execution.


## terraform/ecs-db-task-definition.tf
 - Defined to use FARGATE(AWS managed ECS host) with specified cpu/memory allocation.  
 - the image pulled from Amazon ECR based on the Dockerfile information below.
 - the Docker container exposes the PostgreSQL on port 5432
 - network mode is set to "awsvpc", which will have an elastic network interface and a private IP address  assigned to the task when it runs
 - Password is stored in SSM secured parameter store

## terraform/ecs-db-cluster.tf
 - ECS cluster resource definition for the database

## terraform/ecs-db-service.tf
 - ECS service indicating to just have one task of PostgeSQL running
 - Security group created for ECS Database service, to allow only traffic from ECS Application cluster to the DB Server over tcp port 5432

## database/Dockerfile
- based on postgres v12 docker hub image
- run the SQL file to initiate the table and data
- Image Pushed to Amazon ECR

## database/init.sql
- Creates table BANNER and inserts MESSAGE string "Welcome to DevOps Exercise Demo Page!"

## app/Dockerfile
- based on a public image of PHP-NGINX from docker hub
- installed php postgresql plugins
- sets the static page index.php
- Image Pushed to Amazon ECR

## app/index.php
- sample static page that runs a query against the database
- displays the message on the page when accessed