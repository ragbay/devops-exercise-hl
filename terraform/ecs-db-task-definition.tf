resource "aws_ecs_task_definition" "devops_exercise_db_taskdef" {
  family                   = "devops-exercise-db"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn

  container_definitions = <<DEFINITION
[
  {
    "image": "077288845992.dkr.ecr.us-west-2.amazonaws.com/devops-exercise-db:latest",
    "cpu": 1024,
    "memory": 2048,
    "name": "devops-exercise-db",
    "networkMode": "awsvpc",
    "secrets": [
        {
          "name": "POSTGRES_PASSWORD",
          "valueFrom": "arn:aws:ssm:us-west-2:077288845992:parameter/rvagbay/devops-exercise/postgres/password"
        }
    ],
    "portMappings": [
      {
        "containerPort": 5432,
        "hostPort": 5432
      }
    ]
  }
]
DEFINITION
}
